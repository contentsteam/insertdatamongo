package br.persistencia;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.stream.Stream;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class Main {

	public static void main(String[] args) throws IOException, ParseException {

		String host = "127.0.0.1";
		int port = 27017;
		String database = "bolsafamilia";
		String collection = "pagamentos";
		MongoCollection<Document> metricsCollection = null;

		try {
			MongoClient mongoClient = new MongoClient(host, port);
			MongoDatabase db = mongoClient.getDatabase(database);
			metricsCollection = db.getCollection(collection);

		} catch (Exception e) {
			e.printStackTrace();
		}

		int indice = 0;

		try (Stream<String> lines = Files.lines(Paths.get("bolsaFamiliaFolhaPagamento.csv"))) {
			for (String line : (Iterable<String>) lines::iterator) {
				String[] newLine = line.split("\t");

				if (indice > 0) {
					Document pagamento = new Document();
					pagamento.append("uf", newLine[0]);
					pagamento.append("municipio", newLine[2]);
					pagamento.append("nome", newLine[8]);
					pagamento.append("programa", newLine[9]);
					pagamento.append("valor", newLine[10] != null ?  Double.valueOf(newLine[10].replaceAll(",", "")) : 0.0 );
					pagamento.append("data", new SimpleDateFormat("dd/MM/yyyy").parse("01/" + newLine[11]));

					metricsCollection.insertOne(pagamento);

					System.out.println( indice + " : " + line);
				}
				indice++;
			}
		}
	}
}
